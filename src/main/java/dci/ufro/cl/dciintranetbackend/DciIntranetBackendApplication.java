package dci.ufro.cl.dciintranetbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DciIntranetBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(DciIntranetBackendApplication.class, args);
	}

}
